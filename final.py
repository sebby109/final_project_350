"""
Sebastian Galindo
Cass Randall-Greene
12/8/2021
Purpose: The goal of this research is to see the difference of male and female patients who have symptoms
of heart disease. I would like to see the difference in severity and what they share and do not share.
"""

import pandas as pd
import numpy as np
import io
import requests
import matplotlib.pyplot as plt
import matplotlib
from bs4 import BeautifulSoup
import seaborn as sns

def get_data():
    """
    Gets the data for cleveland for patients that have symptoms of heart disease and which have 
    or do not have the disease.
    """
    
    url = "https://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/"
    fname = "processed.cleveland.data"
    columns =['age', '(1 = male; 0 = female)', 'chest pain', 'blood pressure', 'cholestoral',
            'fasting blood sugar', 'resting electro results', 'max heart rate', 'exercise induced angina', 'thal']
    cleveland_df = pd.read_csv(url + fname, usecols=[0,1,2,3,4,5,6,7,8,13], header=None, squeeze=True)

    cleveland_df.columns = columns
    return cleveland_df

    
def show_m_f(df, males_df, females_df):
    """
    This function displays how many females and males were in the study.
    """

    y_pos = np.arange(2)
    males = 0
    females = 0

    for i in range(len(df)):
        curr_age = df.iloc[i, 1]

        if curr_age == 1.0:
            males += 1
        else:
            females += 1

    labels = ['Male\n' + str(males), 'Female\n' + str(females)]

    fig, (ax1, ax2, ax3) = plt.subplots(3,1)
    labels = ['Male\n' + str(males), 'Female\n' + str(females)]

    ax1.bar(y_pos, [males, females])
    ax1.xaxis.set_ticks(y_pos)
    ax1.set_xticklabels(labels)
    ax1.set_ylabel('Number of patients out of 303', fontsize=12)

    ax2.hist(males_df['age'].astype(int), edgecolor='black', linewidth=0.5)
    ax2.axvline(int(males_df['age'].mean()), color='c', alpha=0.65)
    ax2.set_ylabel("Number of males", fontsize=12)

    ax3.hist(females_df['age'].astype(int), edgecolor='black', linewidth=0.5)
    ax3.axvline(int(females_df['age'].mean()), color='c', alpha=0.65)
    ax3.set_ylabel("Number of females", fontsize=12)
    ax3.set_xlabel("Age")

def get_stats(males, females):
    fig, (ax1, ax2, ax3) = plt.subplots(3,1)

    ax1.hist([males_df['max heart rate'], females_df['max heart rate']], bins = 20, alpha = 0.5)
    ax1.set_xlabel("Heart rate")
    ax1.legend(['Male', 'Female'], loc='upper right')
    
    ax2.hist([males_df['cholestoral'], females_df['cholestoral']], bins = 20, alpha = 0.5)
    ax2.set_xlabel("Cholestoral levels")
    ax2.set_ylabel("Number of people", fontsize=16)
    ax2.legend(['Male', 'Female'], loc='upper right')


    ax3.hist([males_df['blood pressure'], females_df['blood pressure']], bins = 20, alpha = 0.5)
    ax3.set_xlabel("Blood pressure levels")
    ax3.legend(['Male', 'Female'], loc='upper right')

    fig.suptitle('Comaparison of Male/Female symptom levels', fontsize=16)


def correltations(males_df, females_df):
    fig, (ax1, ax2) = plt.subplots(2,1)
    sns.regplot(ax=ax1, x=males_df['blood pressure'], y=males_df['cholestoral'], label=f"Pearson r = {round(males_df['blood pressure'].corr(males_df['cholestoral']), 1)}")
    ax1.set_xlabel('')
    ax1.set_ylabel('Cholestoral level')
    ax1.legend(loc='upper right')

    sns.regplot(ax=ax2, x=females_df['blood pressure'], y=females_df['cholestoral'], label=f"Pearson r = {round(females_df['blood pressure'].corr(females_df['cholestoral']), 1)}")
    ax2.set_xlabel('Blood pressure level')
    ax2.set_ylabel('Cholestoral level')
    ax2.legend(loc='upper right')

df = get_data()

males_df = df[(df['(1 = male; 0 = female)']) == 1.0]
females_df = df[(df['(1 = male; 0 = female)']) == 0.0]

show_m_f(df, males_df, females_df)
get_stats(males_df, females_df)
correltations(males_df, females_df)

plt.show()